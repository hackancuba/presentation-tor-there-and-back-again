# TOR: There and back again

Presentación sobre Tor (The Oninion Router) para el Bazar de Soluciones de CaFeLUG, 17/09/2016.

Un breve repaso por el concepto de la red Tor, su razón de ser, y cómo utilizarla adecuadamente.

**Powered by** [reveal.js](http://lab.hakim.se/reveal-js/).

Puede verse online en [mi web](http://ivan.barreraoro.com.ar/presentation/bazardesoluciones/).

## Licencia
TOR: There and back again por @hackancuba [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es)


**Usted es libre para**:

* Compartir — copiar y redistribuir el material en cualquier medio o formato
* Adaptar — remezclar, transformar y crear a partir del material

Para cualquier propósito, incluso comercialmente. El licenciante no puede revocar estas libertades en tanto usted siga los términos de la licencia

**Bajo los siguientes términos**:

* Atribución — Usted debe darle crédito a esta obra de manera adecuada, proporcionando un enlace a la licencia, e indicando si se han realizado cambios. Puede hacerlo en cualquier forma razonable, pero no de forma tal que sugiera que usted o su uso tienen el apoyo del licenciante.

* CompartirIgual — Si usted mezcla, transforma o crea nuevo material a partir de esta obra, usted podrá distribuir su contribución siempre que utilice lamisma licencia que la obra original.

*No hay restricciones adicionales* — Usted no puede aplicar términos legales ni medidas tecnológicas que restrinjan legalmente a otros hacer cualquier uso permitido por la licencia.

